using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
public class JoystickController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;

    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] public FixedJoystick _joystick;
    [SerializeField] private float _moveSpeed;

    private float forsage = 1f;
    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        _rigidbody = GetComponent<Rigidbody>();
    }
    private void OnEnable()
    {
        playerInputActions.Enable();
    }
    private void OnDisable()
    {
        playerInputActions.Disable();
    }
    private void FixedUpdate()
    {
        playerInputActions.Kart.Forsage.started += context => ForsageOn();
        playerInputActions.Kart.Forsage.canceled += context => ForsageOff();

        _rigidbody.velocity = new Vector3(_joystick.Horizontal * _moveSpeed * forsage, _rigidbody.velocity.y, _joystick.Vertical * _moveSpeed * forsage);
    }
    private void ForsageOn()
    {
        forsage = 3f;
        Debug.Log ("f on");
    }
    private void ForsageOff()
    {
        forsage = 1f;
        Debug.Log ("f off");
    }
}

