using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody playerRigidbody;
    private Vector3 startPosition;
    private JoystickController joystickController;
    public Canvas canvas;

    private float forsage = 1f;

    [SerializeField] private float moveSpeed = 500f;
    private void Awake()
    {
        joystickController =  GetComponent<JoystickController>();
        playerInputActions = new PlayerInputActions();
        playerRigidbody = GetComponent<Rigidbody>();
        startPosition = transform.position;
        canvas.enabled = false;
    }

    private void OnEnable()
    {
        playerInputActions.Enable(); 
    }
    private void OnDisable()
    {
        playerInputActions.Disable();
    }
    private void FixedUpdate()
    {
        playerInputActions.Kart.ChangeControls.performed += context => ChangeControls();
        playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();
        playerInputActions.Kart.Forsage.started += context => ForsageOn();
        playerInputActions.Kart.Forsage.canceled += context => ForsageOff();
                
        Vector2 moveDirection = playerInputActions.Kart.Move.ReadValue<Vector2>();
        Move(moveDirection);
    }
    private void Move(Vector2 direction)
    {
        playerRigidbody.velocity = new Vector3(direction.x * moveSpeed * Time.fixedDeltaTime * forsage, 0f, direction.y * moveSpeed * Time.fixedDeltaTime * forsage);
    }

    private void ResetPosition()
    {
        playerRigidbody.MovePosition(startPosition);
        playerRigidbody.MoveRotation(Quaternion.identity);
    }
    private void ForsageOn()
    {
        forsage = 3f;
    }
    private void ForsageOff()
    {
        forsage = 1f;
    }
    private void ChangeControls()
    {
        if (joystickController.enabled == true)
        {
            joystickController.enabled = false;
            canvas.enabled = false;
        }
        else
        {
            joystickController.enabled = true;
            canvas.enabled = true;
        }
    }
}

